package gabrielaraujo.zapomdb.db;

import android.os.AsyncTask;

import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

import gabrielaraujo.zapomdb.service.AppService;
import gabrielaraujo.zapomdb.service.model.SearchModel;
import gabrielaraujo.zapomdb.service.model.UserFav;
import gabrielaraujo.zapomdb.service.model.response.ResponseDetail;

/**
 * Created by gabrielaraujo on 30/12/16.
 */

public class HelperDB {

    public static void search(String text, AppService.OnSearchCallback callback) {
        new AsyncTask<String, Void, List<SearchModel>>() {

            @Override
            protected List<SearchModel> doInBackground(String... strings) {
                String text = strings[0];
                List<SearchModel> list = new Select().from(SearchModel.class)
                        .where("Title like ?", "%" + text + "%")
                        .orderBy("Title ASC")
                        .execute();
                return list;
            }

            @Override
            protected void onPostExecute(List<SearchModel> list) {
                super.onPostExecute(list);
                if (callback == null) return;
                if (list == null) {
                    callback.onError(null);
                    return;
                }
                callback.onSuccess(list);
            }
        }.execute(text);
    }

    public static void getDetail(String imdbId, AppService.OnGetDetail callback) {
        new AsyncTask<String, Void, ResponseDetail>() {

            @Override
            protected ResponseDetail doInBackground(String... strings) {
                String imdbId = strings[0];
                ResponseDetail responseDetail = new Select().from(ResponseDetail.class)
                        .where("imdbID = ?", imdbId)
                        .executeSingle();
                return responseDetail;
            }

            @Override
            protected void onPostExecute(ResponseDetail responseDetail) {
                super.onPostExecute(responseDetail);
                if (callback == null) return;
                if (responseDetail == null) {
                    callback.onError(null);
                    return;
                }
                callback.onSuccess(responseDetail);
            }
        }.execute(imdbId);
    }

    public static boolean isImdbInfav(String mbdId) {
        return new Select().from(UserFav.class)
                .where("imdbID = ?", mbdId)
                .exists();
    }

    public static void addItemFav(String imdbId) {
        UserFav userFav = new UserFav();
        userFav.setImdbID(imdbId);
        userFav.save();
    }

    public static void deleteItemFav(String imdbId) {
        UserFav userFav = new Select().from(UserFav.class).where("imdbID = ?", imdbId).executeSingle();
        userFav.delete();
    }

    public static void getMyFav(AppService.OnSearchCallback callback) {
        new AsyncTask<Void, Void, List<SearchModel>>() {

            @Override
            protected List<SearchModel> doInBackground(Void... voids) {
                List<SearchModel> list = new Select().from(SearchModel.class).as("s")
                        .join(UserFav.class).as("u")
                        .on("u.imdbID = s.imdbID")
                        .execute();

                return list;
            }

            @Override
            protected void onPostExecute(List<SearchModel> list) {
                super.onPostExecute(list);
                if (callback == null) return;
                if (list == null){
                    callback.onError(null);
                    return;
                }
                callback.onSuccess(list);
            }
        }.execute();
    }
}

package gabrielaraujo.zapomdb.service.retrofit;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import gabrielaraujo.zapomdb.service.model.response.ResponseDetail;
import gabrielaraujo.zapomdb.service.model.response.ResponseSearch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by danielnazareth on 28/12/16.
 */

public interface RestApi {

    @GET("/")
    Call<JsonObject> search(@QueryMap HashMap<String, String> hashMap);

    @GET("/")
    Call<JsonObject> detail(@QueryMap HashMap<String, String> hashMap);
}

package gabrielaraujo.zapomdb.service.model.request;

import java.util.HashMap;

import gabrielaraujo.zapomdb.app.Constants;

/**
 * Created by gabrielaraujo on 28/12/16.
 */

public class RequestDetail {

    public String imdbId;

    public RequestDetail(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public HashMap<String, String> getHash(){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constants.PARAMS.RETURN, Constants.PARAMS.VALUES.JSON);
        hashMap.put(Constants.PARAMS.PLOT, Constants.PARAMS.VALUES.FULL);
        hashMap.put(Constants.PARAMS.IMDB_ID, imdbId);
        return hashMap;
    }
}

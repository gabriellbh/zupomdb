package gabrielaraujo.zapomdb.service.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by gabrielaraujo on 30/12/16.
 */

@Table(name = "favoritos")
public class UserFav extends Model{

    @Column(index = true, name = "imdbID", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String imdbID;

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }
}

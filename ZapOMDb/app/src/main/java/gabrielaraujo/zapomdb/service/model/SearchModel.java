package gabrielaraujo.zapomdb.service.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Created by gabrielaraujo on 29/12/16.
 */

@Table(name = "search")
public class SearchModel extends Model {

    @Expose
    @Column(name = "Title")
    private String Title;

    @Expose
    @Column(name = "Year")
    private String Year;

    @Expose
    @Column(index = true, name = "imdbID", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String imdbID;

    @Expose
    @Column(name = "Type")
    private String Type;

    @Expose
    @Column(name = "Poster")
    private String Poster;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String poster) {
        Poster = poster;
    }
}

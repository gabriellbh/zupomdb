package gabrielaraujo.zapomdb.service.model.response;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Created by gabrielaraujo on 29/12/16.
 */

@Table(name = "detail")
public class ResponseDetail  extends Model{

    @Expose
    @Column(name = "Title")
    private String Title;

    @Expose
    @Column(name = "Year")
    private String Year;

    @Expose
    @Column(name = "Rated")
    private String Rated;

    @Expose
    @Column(name = "Released")
    private String Released;

    @Expose
    @Column(name = "Runtime")
    private String Runtime;

    @Expose
    @Column(name = "Genre")
    private String Genre;

    @Expose
    @Column(name = "Director")
    private String Director;

    @Expose
    @Column(name = "Writer")
    private String Writer;

    @Expose
    @Column(name = "Actors")
    private String Actors;

    @Expose
    @Column(name = "Plot")
    private String Plot;

    @Expose
    @Column(name = "Language")
    private String Language;

    @Expose
    @Column(name = "Country")
    private String Country;

    @Expose
    @Column(name = "Awards")
    private String Awards;

    @Expose
    @Column(name = "Poster")
    private String Poster;

    @Expose
    @Column(name = "Metascore")
    private String Metascore;

    @Expose
    @Column(name = "imdbRating")
    private String imdbRating;

    @Expose
    @Column(name = "imdbVotes")
    private String imdbVotes;

    @Expose
    @Column(index = true, name = "imdbID", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String imdbID;

    @Expose
    @Column(name = "Type")
    private String Type;

    @Expose
    @Column(name = "totalSeasons")
    private String totalSeasons;

    @Expose
    @Column(name = "Response")
    private String Response;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String Year) {
        this.Year = Year;
    }

    public String getRated() {
        return Rated;
    }

    public void setRated(String Rated) {
        this.Rated = Rated;
    }

    public String getReleased() {
        return Released;
    }

    public void setReleased(String Released) {
        this.Released = Released;
    }

    public String getRuntime() {
        return Runtime;
    }

    public void setRuntime(String Runtime) {
        this.Runtime = Runtime;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String Genre) {
        this.Genre = Genre;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String Director) {
        this.Director = Director;
    }

    public String getWriter() {
        return Writer;
    }

    public void setWriter(String Writer) {
        this.Writer = Writer;
    }

    public String getActors() {
        return Actors;
    }

    public void setActors(String Actors) {
        this.Actors = Actors;
    }

    public String getPlot() {
        return Plot;
    }

    public void setPlot(String Plot) {
        this.Plot = Plot;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String Language) {
        this.Language = Language;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getAwards() {
        return Awards;
    }

    public void setAwards(String Awards) {
        this.Awards = Awards;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String Poster) {
        this.Poster = Poster;
    }

    public String getMetascore() {
        return Metascore;
    }

    public void setMetascore(String Metascore) {
        this.Metascore = Metascore;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getImdbVotes() {
        return imdbVotes;
    }

    public void setImdbVotes(String imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getTotalSeasons() {
        return totalSeasons;
    }

    public void setTotalSeasons(String totalSeasons) {
        this.totalSeasons = totalSeasons;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String Response) {
        this.Response = Response;
    }
}

package gabrielaraujo.zapomdb.service.model.request;

import java.util.HashMap;

import gabrielaraujo.zapomdb.app.Constants;

/**
 * Created by gabrielaraujo on 28/12/16.
 */

public class RequestSearch {

    private String text;
    private int page;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public HashMap<String, String> getHash(){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constants.PARAMS.RETURN, Constants.PARAMS.VALUES.JSON);
        hashMap.put(Constants.PARAMS.PLOT, Constants.PARAMS.VALUES.FULL);
        hashMap.put(Constants.PARAMS.PAGE, String.valueOf(page));
        hashMap.put(Constants.PARAMS.SEARCH, text);
        return hashMap;
    }
}

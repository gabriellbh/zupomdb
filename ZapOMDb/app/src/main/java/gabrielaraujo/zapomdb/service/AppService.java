package gabrielaraujo.zapomdb.service;

import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import gabrielaraujo.zapomdb.app.ZapApp;
import gabrielaraujo.zapomdb.db.HelperDB;
import gabrielaraujo.zapomdb.service.model.SearchModel;
import gabrielaraujo.zapomdb.service.model.request.RequestDetail;
import gabrielaraujo.zapomdb.service.model.request.RequestSearch;
import gabrielaraujo.zapomdb.service.model.response.ResponseDetail;
import gabrielaraujo.zapomdb.service.retrofit.RetrofitBase;
import gabrielaraujo.zapomdb.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gabrielaraujo on 29/12/16.
 */

public class AppService {

    private static Call currentCall;

    public interface OnSearchCallback {
        void onSuccess(List<SearchModel> list);

        void onError(Throwable t);
    }

    public interface OnGetDetail {
        void onSuccess(ResponseDetail response);

        void onError(Throwable t);
    }

    public static void search(RequestSearch requestSearch, OnSearchCallback callback) {
        if (!Utils.isOnline(ZapApp.getInstance())) {
            HelperDB.search(requestSearch.getText(), callback);
            return;
        }
        currentCall = RetrofitBase.getInterfaceRetrofit().search(requestSearch.getHash());
        currentCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callback == null) return;
                if (response.isSuccessful()) {
                    if (response.body().has("Search")) {
                        Type typeList = new TypeToken<List<SearchModel>>() {
                        }.getType();
                        callback.onSuccess((List<SearchModel>) Utils.jsonToObject(response.body().get("Search").toString(), typeList));
                    } else {
                        callback.onSuccess(new ArrayList<>());
                    }
                }
                currentCall = null;
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callback == null) return;
                if (call.isCanceled()) return;
                currentCall = null;
                callback.onError(t);
            }
        });
    }

    public static void getDetail(RequestDetail request, OnGetDetail callback) {
        if (!Utils.isOnline(ZapApp.getInstance())) {
            HelperDB.getDetail(request.getImdbId(), callback);
            return;
        }
        currentCall = RetrofitBase.getInterfaceRetrofit().detail(request.getHash());
        currentCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callback == null) return;
                if (response.isSuccessful()) {
                    callback.onSuccess((ResponseDetail) Utils.jsonToObject(response.body().toString(), ResponseDetail.class));
                } else {
                    callback.onError(null);
                }
                currentCall = null;
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callback == null) return;
                if (call.isCanceled()) return;
                currentCall = null;
                callback.onError(t);
            }
        });
    }

    public static void cancelRequest(){
        if (currentCall == null) return;
        currentCall.cancel();
        currentCall = null;
    }
}

package gabrielaraujo.zapomdb.service.retrofit;

import java.util.concurrent.TimeUnit;

import gabrielaraujo.zapomdb.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gabrielaraujo on 22/11/16.
 */

public final class RetrofitBase {

    private Retrofit retrofit;
    private static RestApi interfaceRetrofit;

    public static RestApi getInterfaceRetrofit() {
        if (interfaceRetrofit == null) new RetrofitBase();
        return interfaceRetrofit;
    }

    public RetrofitBase() {
        initRetrofit();
    }

    private void initRetrofit() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logInterceptor);
        }

        String urlBase = BuildConfig.BASE_URL;

        OkHttpClient httpClient = builder.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(urlBase)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        interfaceRetrofit = retrofit.create(RestApi.class);
    }
}

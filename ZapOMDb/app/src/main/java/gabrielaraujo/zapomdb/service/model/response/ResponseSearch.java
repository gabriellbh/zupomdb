package gabrielaraujo.zapomdb.service.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import gabrielaraujo.zapomdb.service.model.SearchModel;

/**
 * Created by gabrielaraujo on 29/12/16.
 */


public class ResponseSearch  {

    @SerializedName("Search")
    private List<SearchModel> Search;

    public List<SearchModel> getSearch() {
        return Search;
    }

    public void setSearch(List<SearchModel> search) {
        Search = search;
    }
}

package gabrielaraujo.zapomdb.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

import gabrielaraujo.zapomdb.R;
import gabrielaraujo.zapomdb.app.Constants;

/**
 * Created by gabrielaraujo on 29/12/16.
 */

public class Utils {

    public static int getType(String type) {
        if (type.equalsIgnoreCase(Constants.TypeItem.FILME))
            return R.string.filme;
        else if (type.equalsIgnoreCase(Constants.TypeItem.EPISODIO))
            return R.string.episodio;
        else
            return R.string.serie;
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Object jsonToObject(String obj, Class<?> classModel) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .create();
        return gson.fromJson(obj, classModel);
    }

    public static Object jsonToObject(String obj, Type classModel) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .create();
        return gson.fromJson(obj, classModel);
    }

    public static String objectToString(Object obj) {
        try {
            if (obj == null) {
                return "";
            }
            Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();
            return gson.toJson(obj);
        } catch (Exception ex) {
            return "";
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void showDialog(Context context, String msg, MaterialDialog.SingleButtonCallback callback){
        new MaterialDialog.Builder(context)
                .content(msg)
                .positiveText(R.string.ok)
                .onPositive(callback)
                .show();
    }
}

package gabrielaraujo.zapomdb.presenter.search;

import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import gabrielaraujo.zapomdb.app.Constants;
import gabrielaraujo.zapomdb.service.AppService;
import gabrielaraujo.zapomdb.service.model.SearchModel;
import gabrielaraujo.zapomdb.service.model.request.RequestSearch;
import gabrielaraujo.zapomdb.view.adapter.SearchAdapter;

/**
 * Created by gabrielaraujo on 28/12/16.
 */

@InjectViewState
public class SearchPresenter extends MvpPresenter<SearchPresenterCallback> {

    private Handler handlerSearch;
    private Runnable runnableSearch;
    private SearchAdapter adapter;

    private String textSearch;
    private int page = 1;
    private boolean isMore;

    public SearchPresenter() {
        init();
    }

    private void init() {
        handlerSearch = new Handler();
        runnableSearch = () -> {
            if (adapter != null) adapter.clear();
            search(textSearch);
            page = 1;
        };
        getViewState().OnObserverEdtSearch();
        getViewState().setScrollList(getScrollListener());
    }

    public void verifyEdtSearch(CharSequence text) {
        handlerSearch.removeCallbacks(runnableSearch);
        if (text.length() >= Constants.MIN_SEARCH) {
            textSearch = text.toString();
            startTimeSearch();
        } else if (TextUtils.isEmpty(text)) {
            getViewState().onLoading(false);
            AppService.cancelRequest();
        }
    }

    public void clearAdapter(){
        if (adapter != null) adapter.clear();
    }

    private void startTimeSearch() {
        handlerSearch.postDelayed(runnableSearch, 1000);
    }

    public void search(String text) {
        AppService.cancelRequest();
        RequestSearch request = new RequestSearch();
        request.setPage(page);
        request.setText(text);
        getViewState().onLoading(true);
        AppService.search(request, new AppService.OnSearchCallback() {
            @Override
            public void onSuccess(List<SearchModel> list) {
                getViewState().onLoading(false);
                if (list.size() == 10) {
                    isMore = true;
                    page++;
                } else {
                    isMore = false;
                }
                initAdapter(list);
            }

            @Override
            public void onError(Throwable t) {
                getViewState().onLoading(false);
            }
        });
    }

    private void initAdapter(List<SearchModel> list) {
        if (list != null && list.size() > 0) {
            saveBD(list);
            if (adapter == null) {
                adapter = new SearchAdapter(list);
                getViewState().setListAdapter(adapter);
            } else {
                adapter.addList(list);
                getViewState().hideMsgNaoEncontrado();
            }
        } else {
            if (adapter != null) adapter.clear();
            getViewState().showMsgNaoEncontrado();
        }
    }

    public RecyclerView.OnScrollListener getScrollListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    int pastVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                    if (((visibleItemCount + pastVisibleItems) >= totalItemCount)) {
                        if (isMore) {
                            isMore = false;
                            search(textSearch);
                        }
                    }
                }
            }
        };
    }

    private void saveBD(List<SearchModel> list) {
        for (SearchModel search : list) {
            search.save();
        }
    }

}

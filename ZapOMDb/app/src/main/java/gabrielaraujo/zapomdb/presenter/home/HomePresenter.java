package gabrielaraujo.zapomdb.presenter.home;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import gabrielaraujo.zapomdb.db.HelperDB;
import gabrielaraujo.zapomdb.service.AppService;
import gabrielaraujo.zapomdb.service.model.SearchModel;
import gabrielaraujo.zapomdb.view.adapter.SearchAdapter;

/**
 * Created by gabrielaraujo on 30/12/16.
 */

@InjectViewState
public class HomePresenter extends MvpPresenter<HomePresenterCallback>{

    private SearchAdapter adapter;

    public void getFav(){
        getViewState().onLoading(true);
        HelperDB.getMyFav(new AppService.OnSearchCallback() {
            @Override
            public void onSuccess(List<SearchModel> list) {
                getViewState().onLoading(false);
                if (list.size() > 0){
                    initAdapter(list);
                    getViewState().hidePlaceholder();
                }else{
                    getViewState().showPlaceholder();
                }
            }

            @Override
            public void onError(Throwable t) {
                getViewState().onLoading(false);
                getViewState().showPlaceholder();
            }
        });
    }

    private void initAdapter(List<SearchModel> list){
        adapter = new SearchAdapter(list);
        getViewState().setList(adapter);
    }
}

package gabrielaraujo.zapomdb.presenter.home;

import com.arellomobile.mvp.MvpView;

import gabrielaraujo.zapomdb.view.adapter.SearchAdapter;

/**
 * Created by gabrielaraujo on 30/12/16.
 */

public interface HomePresenterCallback extends MvpView {

    void showPlaceholder();
    void hidePlaceholder();

    void setList(SearchAdapter adapter);

    void onLoading(boolean status);

}

package gabrielaraujo.zapomdb.presenter.search;

import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.MvpView;

import gabrielaraujo.zapomdb.view.adapter.SearchAdapter;

/**
 * Created by gabrielaraujo on 28/12/16.
 */

public interface SearchPresenterCallback extends MvpView {

    void OnObserverEdtSearch();

    void onLoading(boolean status);

    void setListAdapter(SearchAdapter adapter);

    void showMsgNaoEncontrado();
    void hideMsgNaoEncontrado();

    void setScrollList(RecyclerView.OnScrollListener listener);
}

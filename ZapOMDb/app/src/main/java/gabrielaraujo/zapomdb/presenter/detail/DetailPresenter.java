package gabrielaraujo.zapomdb.presenter.detail;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import gabrielaraujo.zapomdb.R;
import gabrielaraujo.zapomdb.db.HelperDB;
import gabrielaraujo.zapomdb.service.AppService;
import gabrielaraujo.zapomdb.service.model.request.RequestDetail;
import gabrielaraujo.zapomdb.service.model.response.ResponseDetail;

/**
 * Created by gabrielaraujo on 29/12/16.
 */

@InjectViewState
public class DetailPresenter extends MvpPresenter<DetailPresenterCallback> {

    private String imdbId;
    private ResponseDetail responseDetail;
    private boolean isFav;

    public DetailPresenter(String imdbId) {
        this.imdbId = imdbId;
        getDetail();
    }

    private void getDetail() {
        getViewState().onLoading(true);
        AppService.getDetail(new RequestDetail(imdbId), new AppService.OnGetDetail() {
            @Override
            public void onSuccess(ResponseDetail response) {
                getViewState().onLoading(false);
                responseDetail = response;
                responseDetail.save();
                init();
            }

            @Override
            public void onError(Throwable t) {
                getViewState().onLoading(false);
                if (t == null) {
                    getViewState().onShowError(R.string.error_generic);
                } else {
                    getViewState().onShowError(t.getMessage());
                }
            }
        });
    }

    private void init() {
        getViewState().setTextToolbar(responseDetail.getTitle());
        getViewState().setImageBlur(responseDetail.getPoster());
        getViewState().setImageDetail(responseDetail.getPoster());
        getViewState().setTitle(responseDetail.getTitle());
        getViewState().setAno(responseDetail.getYear().replace("–", ""));
        getViewState().setPlot(responseDetail.getPlot());
        getViewState().setDisponivel(responseDetail.getReleased());
        getViewState().setDuracao(responseDetail.getRuntime());
        getViewState().setGenero(responseDetail.getGenre());
        getViewState().setEscritor(responseDetail.getWriter());
        getViewState().setDiretor(responseDetail.getDirector());

        if (TextUtils.isEmpty(responseDetail.getImdbRating()) || responseDetail.getImdbRating().equalsIgnoreCase("0")) {
            getViewState().hideContainerAvaliacao();
        } else {
            getViewState().setAvaliacao(String.format("%s/10", responseDetail.getImdbRating()));
            getViewState().setAvaliacaoTotal(responseDetail.getImdbVotes());
        }

        if (TextUtils.isEmpty(responseDetail.getAwards())) {
            getViewState().hidePremio();
        } else {
            getViewState().setPremio(responseDetail.getAwards());
        }
    }

    public void clickFav(){
        if (isFav) {
            getViewState().showDialogDeleteFav();
        }else{
            getViewState().showDialogAddFav();
        }
    }

    public void verifyStatusFav(){
        if (HelperDB.isImdbInfav(imdbId)){
            getViewState().setIconFavOn();
            isFav = true;
        }else{
            getViewState().setIconFavOff();
            isFav = false;
        }
    }

    public void addItemToFav(){
        HelperDB.addItemFav(imdbId);
        getViewState().setIconFavOn();
    }

    public void deleteItemToFav(){
        HelperDB.deleteItemFav(imdbId);
        getViewState().setIconFavOff();
    }
}

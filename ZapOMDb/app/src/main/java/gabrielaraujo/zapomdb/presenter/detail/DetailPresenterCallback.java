package gabrielaraujo.zapomdb.presenter.detail;

import android.content.Context;

import com.arellomobile.mvp.MvpView;

/**
 * Created by gabrielaraujo on 29/12/16.
 */

public interface DetailPresenterCallback extends MvpView {

    void onLoading(boolean status);
    void onShowError(String msg);
    void onShowError(int msg);


    void setTextToolbar(String text);

    void setImageBlur(String pathImage);
    void setImageDetail(String pathImage);

    void setPlot(String text);
    void setTitle(String text);
    void setAno(String text);
    void setDisponivel(String text);
    void setDuracao(String text);
    void setGenero(String text);
    void setEscritor(String text);
    void setDiretor(String text);

    void hideContainerAvaliacao();
    void setAvaliacao(String text);
    void setAvaliacaoTotal(String total);

    void hidePremio();
    void setPremio(String text);

    void setIconFavOn();
    void setIconFavOff();

    void showDialogAddFav();
    void showDialogDeleteFav();
}

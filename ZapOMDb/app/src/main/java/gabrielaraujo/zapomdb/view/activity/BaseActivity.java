package gabrielaraujo.zapomdb.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.arellomobile.mvp.MvpAppCompatActivity;

import gabrielaraujo.zapomdb.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gabrielaraujo on 28/12/16.
 */

public class BaseActivity extends MvpAppCompatActivity {

    private Toolbar toolbar;
    protected MaterialDialog materialProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDialog();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void setUpToolbarText(int title, boolean isBack) {
        setUpToolbarText(getString(title), isBack);
    }

    public void setUpToolbarText(String title, boolean isBack) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            if (!TextUtils.isEmpty(title)) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                setTitleToolbar(toolbar, title);
            } else {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
            if (isBack) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
            }
        }
    }

    public void setTitleToolbar(Toolbar toolbar, String title) {
        TextView textView = (TextView) toolbar.findViewById(R.id.txt_toolbar);
        textView.setText(title);
        textView.setVisibility(View.VISIBLE);
    }

    public void openAcitivity(Class<?> classOpen) {
        Intent intent = new Intent(this, classOpen);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initDialog(){
        materialProgress = new MaterialDialog.Builder(this)
                .content(R.string.aguarde)
                .progress(true, 0)
                .cancelable(false)
                .build();
    }
}

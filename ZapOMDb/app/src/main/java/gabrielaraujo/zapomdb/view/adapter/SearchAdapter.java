package gabrielaraujo.zapomdb.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import gabrielaraujo.zapomdb.R;
import gabrielaraujo.zapomdb.service.model.SearchModel;
import gabrielaraujo.zapomdb.utils.Utils;
import gabrielaraujo.zapomdb.view.activity.DetailActivity;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by gabrielaraujo on 29/12/16.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchHolder> {

    private List<SearchModel> list;
    private Context context;

    public SearchAdapter(List<SearchModel> list) {
        this.list = list;
    }

    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search, null);
        return new SearchHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchHolder holder, int position) {
        SearchModel searchModel = list.get(position);
        String text = String.format("%s (%s)", searchModel.getTitle(), searchModel.getYear().replace("–", ""));
        holder.txtName.setText(text);
        holder.txtType.setText(Utils.getType(searchModel.getType()));
        Glide.with(context)
                .load(searchModel.getPoster())
                .bitmapTransform(new CropCircleTransformation(context))
                .crossFade()
                .into(holder.img);
        holder.itemView.setOnClickListener(view -> DetailActivity.open(context, searchModel.getImdbID()));
    }

    public void addList(List<SearchModel> list){
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void clear(){
        this.list.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class SearchHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.txt_name)
        TextView txtName;
        @BindView(R.id.txt_type)
        TextView txtType;

        SearchHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

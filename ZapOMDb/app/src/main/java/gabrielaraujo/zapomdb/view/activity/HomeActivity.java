package gabrielaraujo.zapomdb.view.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import gabrielaraujo.zapomdb.R;
import gabrielaraujo.zapomdb.presenter.home.HomePresenter;
import gabrielaraujo.zapomdb.presenter.home.HomePresenterCallback;
import gabrielaraujo.zapomdb.view.adapter.SearchAdapter;

public class HomeActivity extends BaseActivity implements HomePresenterCallback, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.list_fav)
    RecyclerView listFav;
    @BindView(R.id.txt_fav_empty)
    TextView txtFavEmpty;
    @BindView(R.id.activity_home)
    RelativeLayout activityHome;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.container_list_favoritos)
    LinearLayout containerListFavoritos;

    @InjectPresenter
    HomePresenter presenter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setUpToolbarText(R.string.app_name, false);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getFav();
    }

    private void init() {
        swipe.setColorSchemeResources(R.color.colorPrimary);
        swipe.setOnRefreshListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            openAcitivity(SearchAcitivty.class);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showPlaceholder() {
        txtFavEmpty.setVisibility(View.VISIBLE);
        containerListFavoritos.setVisibility(View.GONE);
    }

    @Override
    public void hidePlaceholder() {
        txtFavEmpty.setVisibility(View.GONE);
        containerListFavoritos.setVisibility(View.VISIBLE);
    }

    @Override
    public void setList(SearchAdapter adapter) {
        listFav.setLayoutManager(new LinearLayoutManager(this));
        listFav.setAdapter(adapter);
    }

    @Override
    public void onLoading(boolean status) {
        swipe.setRefreshing(status);
    }

    @Override
    public void onRefresh() {

    }
}

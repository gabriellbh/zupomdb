package gabrielaraujo.zapomdb.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import gabrielaraujo.zapomdb.R;
import gabrielaraujo.zapomdb.app.Constants;
import gabrielaraujo.zapomdb.presenter.detail.DetailPresenter;
import gabrielaraujo.zapomdb.presenter.detail.DetailPresenterCallback;
import gabrielaraujo.zapomdb.utils.Utils;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by gabrielaraujo on 29/12/16.
 */

public class DetailActivity extends BaseActivity implements DetailPresenterCallback {

    @BindView(R.id.img_blur)
    ImageView imgBlur;
    @BindView(R.id.img_details)
    ImageView imgDetails;
    @BindView(R.id.txt_plot)
    TextView txtPlot;
    @BindView(R.id.txt_titulo)
    TextView txtTitulo;
    @BindView(R.id.txt_ano)
    TextView txtAno;
    @BindView(R.id.txt_disponivel)
    TextView txtDisponivel;
    @BindView(R.id.txt_duracao)
    TextView txtDuracao;
    @BindView(R.id.txt_genero)
    TextView txtGenero;
    @BindView(R.id.txt_escritor)
    TextView txtEscritor;
    @BindView(R.id.txt_diretor)
    TextView txtDiretor;
    @BindView(R.id.txt_avaliacao)
    TextView txtAvaliacao;
    @BindView(R.id.txt_total_avaliacao)
    TextView txtTotalAvaliacao;
    @BindView(R.id.container_avaliacao)
    LinearLayout containerAvaliacao;
    @BindView(R.id.txt_premio)
    TextView txtPremio;

    @InjectPresenter
    DetailPresenter presenter;

    private MenuItem menuFav;


    @ProvidePresenter
    public DetailPresenter provideDetailsPresenter() {
        return new DetailPresenter(getIntent().getStringExtra(Constants.EXTRA_IMDB_ID));
    }

    public static void open(Context context, String imdbId) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(Constants.EXTRA_IMDB_ID, imdbId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setUpToolbarText(null, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_fav, menu);
        menuFav = menu.findItem(R.id.action_fav);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_fav){
            presenter.clickFav();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (presenter != null) presenter.verifyStatusFav();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onLoading(boolean status) {
        if (status) {
            materialProgress.show();
        } else {
            materialProgress.dismiss();
        }
    }

    @Override
    public void onShowError(String msg) {
        Utils.showDialog(this, msg, (dialog, which) -> finish());
    }

    @Override
    public void onShowError(int msg) {
        Utils.showDialog(this, getString(msg), (dialog, which) -> finish());
    }

    @Override
    public void setTextToolbar(String text) {
        setUpToolbarText(text, true);
    }

    @Override
    public void setImageBlur(String pathImage) {
        Glide.with(this)
                .load(pathImage)
                .bitmapTransform(new BlurTransformation(this))
                .into(imgBlur);
    }

    @Override
    public void setImageDetail(String pathImage) {
        Glide.with(this).load(pathImage).bitmapTransform(new RoundedCornersTransformation(this, 10, 0)).into(imgDetails);
    }

    @Override
    public void setPlot(String text) {
        txtPlot.setText(text);
    }

    @Override
    public void setTitle(String text) {
        txtTitulo.setText(text);
    }

    @Override
    public void setAno(String text) {
        txtAno.setText(text);
    }

    @Override
    public void setDisponivel(String text) {
        txtDisponivel.setText(text);
    }

    @Override
    public void setDuracao(String text) {
        txtDuracao.setText(text);
    }

    @Override
    public void setGenero(String text) {
        txtGenero.setText(text);
    }

    @Override
    public void setEscritor(String text) {
        txtEscritor.setText(text);
    }

    @Override
    public void setDiretor(String text) {
        txtDiretor.setText(text);
    }

    @Override
    public void hideContainerAvaliacao() {
        containerAvaliacao.setVisibility(View.GONE);
    }

    @Override
    public void setAvaliacao(String text) {
        txtAvaliacao.setText(text);
    }

    @Override
    public void setAvaliacaoTotal(String total) {
        txtTotalAvaliacao.setText(total);
    }

    @Override
    public void hidePremio() {
        txtPremio.setVisibility(View.GONE);
    }

    @Override
    public void setPremio(String text) {
        txtPremio.setText(text);
    }

    @Override
    public void setIconFavOn() {
        menuFav.setIcon(R.drawable.ic_favorite_on);
    }

    @Override
    public void setIconFavOff() {
        menuFav.setIcon(R.drawable.ic_favorite_off);
    }

    @Override
    public void showDialogAddFav() {
        new MaterialDialog.Builder(this)
                .content(String.format(getString(R.string.deseja_add), txtTitulo.getText().toString()))
                .positiveText(R.string.sim)
                .negativeText(R.string.nao)
                .onPositive((dialog, which) -> presenter.addItemToFav())
                .show();
    }

    @Override
    public void showDialogDeleteFav() {
        new MaterialDialog.Builder(this)
                .content(String.format(getString(R.string.deseja_delte), txtTitulo.getText().toString()))
                .positiveText(R.string.sim)
                .negativeText(R.string.nao)
                .onPositive((dialog, which) -> presenter.deleteItemToFav())
                .show();
    }
}

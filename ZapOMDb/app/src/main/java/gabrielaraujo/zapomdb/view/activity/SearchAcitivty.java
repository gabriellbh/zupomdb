package gabrielaraujo.zapomdb.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import gabrielaraujo.zapomdb.R;
import gabrielaraujo.zapomdb.presenter.search.SearchPresenter;
import gabrielaraujo.zapomdb.presenter.search.SearchPresenterCallback;
import gabrielaraujo.zapomdb.utils.Utils;
import gabrielaraujo.zapomdb.view.adapter.SearchAdapter;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by gabrielaraujo on 28/12/16.
 */

public class SearchAcitivty extends BaseActivity implements SearchPresenterCallback, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.edt_search)
    MaterialEditText edtSearch;
    @BindView(R.id.list_result)
    RecyclerView listResult;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    @InjectPresenter
    SearchPresenter presenter;
    @BindView(R.id.txt_msg_not_found)
    TextView txtMsgNotFound;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setUpToolbarText(R.string.pesquisar, true);
        swipe.setOnRefreshListener(this);
    }

    @Override
    public void OnObserverEdtSearch() {
        RxTextView.textChanges(edtSearch)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(presenter::verifyEdtSearch);
    }

    @Override
    public void onLoading(boolean status) {
        swipe.setRefreshing(status);
    }

    @Override
    public void setListAdapter(SearchAdapter adapter) {
        Utils.hideKeyboard(this, edtSearch);
        listResult.setLayoutManager(new LinearLayoutManager(this));
        listResult.setAdapter(adapter);
        txtMsgNotFound.setVisibility(View.GONE);
        listResult.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMsgNaoEncontrado() {
        Utils.hideKeyboard(this, edtSearch);
        txtMsgNotFound.setVisibility(View.VISIBLE);
        listResult.setVisibility(View.GONE);
    }

    @Override
    public void hideMsgNaoEncontrado() {
        Utils.hideKeyboard(this, edtSearch);
        txtMsgNotFound.setVisibility(View.GONE);
        listResult.setVisibility(View.VISIBLE);
    }

    @Override
    public void setScrollList(RecyclerView.OnScrollListener listener) {
        listResult.addOnScrollListener(listener);
    }

    @Override
    public void onRefresh() {
        presenter.clearAdapter();
        presenter.search(edtSearch.getText().toString());
    }
}

package gabrielaraujo.zapomdb.app;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

import gabrielaraujo.zapomdb.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by gabrielaraujo on 28/12/16.
 */

public class ZapApp extends Application {

    private static ZapApp instance;

    public static ZapApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        init();
    }

    private void init() {
        ActiveAndroid.initialize(this);
        setCustomFont();
    }

    private void setCustomFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.avenir_book))
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}

package gabrielaraujo.zapomdb.app;

/**
 * Created by gabrielaraujo on 28/12/16.
 */

public class Constants {

    public static final int MIN_SEARCH = 2;
    public static final String EXTRA_IMDB_ID = "extra_imdb_id";

    public static final class PARAMS{
        public static final String RETURN = "r";
        public static final String PAGE = "page";
        public static final String PLOT = "plot";
        public static final String SEARCH = "s";
        public static final String IMDB_ID = "i";

        public static final class VALUES{
            public static final String JSON = "json";
            public static final String FULL = "full";
        }
    }

    public static final class TypeItem{
        public static final String FILME = "movie";
        public static final String SERIE = "series";
        public static final String EPISODIO = "episode";
    }


}
